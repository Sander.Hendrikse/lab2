package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    private List<FridgeItem> items;
    private List<FridgeItem> expierdFood;
    public Fridge() {
        this.items = new ArrayList<>();
        this.expierdFood = new ArrayList<>();
    }

    // Returns the number of items currently in the fridge
    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    private int max_size = 20;
    // Returns the total number of items there is space for in the fridge
    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (items.size() < max_size) {
            items.add(item);
            return true;
        }  return false; 
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!items.contains(item)) {
            throw new NoSuchElementException();
        } 
        items.remove(item);
    }

    @Override
    public void emptyFridge() {
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        for (FridgeItem item: items) {
            if(item.hasExpired()) {
                expierdFood.add(item);
            }
        }
        items.removeIf(FridgeItem::hasExpired);

        return expierdFood;
    }
}
